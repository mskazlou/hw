import xml.etree.ElementTree as ET
from copy import deepcopy
from datetime import datetime
""" 
    напечатайте id всех книг
    Посчитайте кол-во книг и напечатайте
    Отсортируйте книги по цене
    Отсортируйте книгши по дате выпуска
    Напечатайте книги выпущенные в 2000 году
    Найти книги в жанре Computer. Напечатайте их автора, годы выпуска и описание
"""


def ids_output(root_var):
    list_of_ids = []
    for element in root_var:
        list_of_ids.append(element.attrib['id'])

    print('1. Вывожу id всех книг: ')
    print(*list_of_ids)


def books_count_output(root_var):
    print('2. Вывожу количество всех книг: ')
    print(f'{len(root_var)} книг.')


def books_price_sorting(root_var):
    """Функция выводит словарь в виде Имя_книги: цена."""
    temporary_dict = {}

    for num, child in enumerate(root_var):
        key = child[1].text  # достаю имя книги в каждом каталога
        value = child[3].text  # достаю дату в каждом элементе каталога
        temporary_dict[key] = float(value)

    sorted_tuple = sorted(temporary_dict.items(), key=lambda  item: item[1])
    sorted_dict = {key: value for key, value in sorted_tuple}
    print('3. Вывожу отсортированный по цене словарь: ')
    print(sorted_dict)


def books_date_sorting(root_var):
    """Функция выводит словарь в виде Имя_книги: дата.
    В исходнике даты уже упорядочены, но я вручную менял данные и
    функция рабочая.
    P.S. безумное задание :)"""

    temporary_dict = {}     # создаю пустой словарь

    for child in root_var:
        key = child[1].text     # достаю имя книги в каждом каталога
        value = child[4].text   # достаю дату в каждом элементе каталога
        temporary_dict[key] = datetime.strptime(value, "%Y-%m-%d")  # перевел строчный формат в формат даты

    sorted_tuple = sorted(temporary_dict.items(), key=lambda item: item[1])     # создание отсортированных
    # наборов данных в кортеже

    sorted_dict = {key: datetime.strftime(value, "%Y-%m-%d") for key, value in sorted_tuple}    # создание сортированного
    # словаря

    print('4. Вывожу отсортированный по дате словарь: ')
    print(sorted_dict)


def is_2000_year_book(root_var):
    """Функция выводит книги, датированные 2000 годом"""
    temporary_dict = {}     # создаю пустой словарь
    for child in root_var:
        key = child[1].text  # достаю имя книги в каждом каталога
        value = child[4].text  # достаю дату в каждом элементе каталога
        temporary_dict[key] = value

    millennium_dict = {}
    for key, value in temporary_dict.items():
        if '2000' in temporary_dict[key]:
            millennium_dict[key] = value

    if millennium_dict:
        print('5. Вывожу словарь с именами книг 2000 года: ')
        print(millennium_dict)
    else:
        print('Нет книг 2000 года!')


def check_book_genre(root_var, genre=None):
    """Функция выводит книги и информацию о них по типу жанра."""
    list_of_books = []
    if genre is None:
        genre = 'Computer'

    for child in root_var:
        if genre == child[2].text:
            list_of_books.append(child)

    if list_of_books:
        print(f'6. Вывожу книги в жанре {genre} и информацию о них: ')
        for element in list_of_books:
            print(element[0].text, element[4].text, element[5].text)
    else:
        print(f'Книг в жанре {genre} нет.')


if __name__ == '__main__':

    root = ET.parse('content.xml').getroot()
    ids_output(root)
    print('*' * 40)

    books_count_output(root)
    print('*' * 40)

    books_price_sorting(root)
    print('*' * 40)

    books_date_sorting(root)
    print('*' * 40)

    is_2000_year_book(root)
    print('*' * 40)

    check_book_genre(root)
    print('*' * 40)