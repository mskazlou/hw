import unittest
from classes_hw import School, Student

test_list_of_student = {
    Student("Kozlov", "M.S.", 1, {'p.e': 4, 'math': 5, 'literature': 6, 'geography': 7, 'biology': 8}),
    Student("Ignatenko", "M.V.", 1, {'p.e': 7, 'math': 7, 'literature': 7, 'geography': 7, 'biology': 7}),
    Student("Ignatenko", "O.V.", 2, {'p.e': 8, 'math': 8, 'literature': 8, 'geography': 8, 'biology': 8}),
    Student("Kazakov", "A.G.", 2, {'p.e': 9, 'math': 9, 'literature': 9, 'geography': 9, 'biology': 9}),
    Student("Zaiko", "O.S.", 3, {'p.e': 10, 'math': 10, 'literature': 10, 'geography': 10, 'biology': 10}),
    Student("Kurlovich", "A.E.", 3, {'p.e': 6, 'math': 6, 'literature': 6, 'geography': 6, 'biology': 6})
}


class TestStudentClass(unittest.TestCase):

    def setUp(self):
        self.student = Student("Kozlov", "M.S.", 1,
                               {'p.e': 4, 'math': 5, 'literature': 6, 'geography': 7, 'biology': 8})

    def test_student_name(self):
        self.assertEqual(self.student.first_name, 'Kozlov'), "first name should be Kozlov "

    def test_student_initials(self):
        self.assertEqual(self.student.initials, 'M.S.'), "student initials should be equal to M.S."

    def test_student_group_num(self):
        assert self.student.group_num == 1, 'group num should be equal to 1'

    def test_student_marks(self):
        self.assertEqual(self.student.marks, {'p.e': 4, 'math': 5, 'literature': 6, 'geography': 7, 'biology': 8}), \
        'marks should be equal to 4, 5, 6, 7, 8'


class TestSchoolClass(unittest.TestCase):
    def setUp(self):
        self.school = School()
        for num in range(1, 4):
            self.school.create_group_num(num)

        for student in test_list_of_student:
            self.school.add_student(student)

    def test_group_num_count(self):
        """Тест проверяет количество созданных групп"""
        goups_count = (len(self.school.school_class.keys()))
        self.assertEqual(goups_count, 3), 'groups count should be equal to 3'

    def test_group_names(self):
        """Тест проверяет имена созданных групп"""
        expected_result = ['1', '2', '3']
        test_result = []
        for key in self.school.school_class.keys():
            test_result.append(key)
        self.assertEqual(test_result, expected_result), 'group names should be equal to "1", "2", "3" '

    @unittest.expectedFailure
    def test_group_names_fail(self):

        """Тест для пометки с помощью Xfail"""

        expected_result = ['7', '7', '7']
        test_result = []
        for key in get_school.school_class.keys():
            test_result.append(key)
        assert test_result == expected_result, 'group names should be equal to "1", "2", "3" '


class TestSchoolMethods(unittest.TestCase):
    """Test Case for School class methods"""

    def setUp(self):
        self.school = School()
        for num in range(1, 4):
            self.school.create_group_num(num)

        for student in test_list_of_student:
            self.school.add_student(student)

    def test_average_5_6(self):

        """тест проверяет имена учеников имеющих средний балл = 5-6"""

        expected_list = ['Kozlov', 'Kurlovich']
        test_list = []
        for value in self.school.school_class.values():
            for student in value:
                average = sum(student.marks.values()) / 5

                if 5 <= average <= 6:
                    test_list.append(student.first_name)

        self.assertEqual(expected_list, test_list), "test_list != 'Kozlov', 'Kurlovich'"

    def test_average_7(self):

        """тест проверяет инициалы учеников имеющих средний балл = 7+"""

        expected_list = ["M.V.", "A.G.", "O.V.", "O.S."]
        test_list = []

        for value in self.school.school_class.values():
            for student in value:
                average = sum(student.marks.values()) / 5

                if average >= 7:
                    test_list.append(student.initials)

        assert expected_list.sort() == test_list.sort(), '(test_list != "M.V.", "A.G.", "O.V.", "O.S.")'

    def test_get_mates(self):

        """тест сравнивает учеников одной группы"""

        expected_list = ["Kazakov", "Ignatenko"]
        test_list = []

        group_num = '2'  # Проверяю наличие одноклассников группы №2
        students_group = self.school.school_class[group_num]

        for student in students_group:
            test_list.append(student.first_name)

        assert expected_list.sort() == test_list.sort(), "test_list != 'Ignatenko', 'Kazakov'"


if __name__ == '__main__':
    unittest.main()
