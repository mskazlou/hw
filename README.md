в TestCases находится conftest.py. НЕОБХОДИМО ВРУЧНУЮ СМЕНИТЬ ССЫЛКУ хромдрайвера.
(не добавляю хроминсталл менеджер, чтобы не плодить мусор в твоем кампутере)

в resources текстовый файл с локаторами. Для корректной работы парсера 
 необходимо соблюдать одну пустую строку между группами локаторов

в utilities:
1. парсер .txt файла. Был смысл добавить в парсер функции по типу 
get_css_locator / get_xpath_locator и т.д. , но это все будет кучей однообразного кода.

2. custom_logger с настройками логгера для импорта готового логгера в тесты.


How To Run Tests
----------------

1) Change chromedriver path variable in conftest.py file 

2) Run tests:

change dir to "TestCases"
    ```bash
    pytest -v test_login.py
    ```

