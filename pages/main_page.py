from pages.base_page import BasePage
from pages.locators import MainPageLocators
from pages.locators import StoreLocators


class MainPage(BasePage):

    def go_to_login_page(self):
        login_button = self.find_element(*MainPageLocators.LOGIN_LINK)
        self.click_on_element(login_button)

    def should_be_login_link(self):
        return self.is_element_present(*MainPageLocators.INCORRECT_LINK)

    def get_the_basket(self):
        basket_button = self.find_element(*MainPageLocators.BASKET_LINK)
        self.click_on_element(basket_button)

    def get_basket_text(self):

        try:
            element = self.find_element(*MainPageLocators.BASKET_TEXT_ELEMENT)

            self.click_on_element(element)
        except Exception as e:
            print('Error: {0}'.format(e))
        return element.text

    def go_to_main_page(self):
        main_page_link_element = self.find_element(*StoreLocators.MAIN_PAGE_LINK)
        self.click_on_element(main_page_link_element)

    def pass_the_keys_to_the_search_field(self):
        self.send_keys(*StoreLocators.SEARCH_FILED)

        search_btn = self.find_element(*StoreLocators.SEARCH_BUTTON)
        self.click_on_element(search_btn)
