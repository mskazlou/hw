from selenium.webdriver.common.by import By
from utilities.locators_reader import LocatorsFinder
from utilities.custom_logger import LogGen


class TestLogin:
    logger = LogGen.loggen()
    url = LocatorsFinder().get_url()  # returns https://www.saucedemo.com/
    username = LocatorsFinder().get_login()  # returns standart_user
    password = LocatorsFinder().get_pass()  # returns secret_sauce

    def test_login(self, web_driver):
        username_field_locator = LocatorsFinder().get_locator('username_field')
        password_field_locator = LocatorsFinder().get_locator('password_field')
        login_btn_locator = LocatorsFinder().get_locator('login_button')
        products_table_locator = LocatorsFinder().get_locator('products_table')

        self.logger.info('************* TestLogin *************')
        self.logger.info('************* Verifying login procedure *************')
        self.driver = web_driver
        self.driver.get(self.url)

        # прохожу логин процедуру
        self.driver.find_element(By.ID, username_field_locator).send_keys(self.username)
        self.driver.find_element(By.NAME, password_field_locator).send_keys(self.password)
        self.driver.find_element(By.CSS_SELECTOR, login_btn_locator).click()

        # Получаю текст из лого просто ради фиксирования какого-то результата
        table_text = self.driver.find_element(By.CSS_SELECTOR, products_table_locator).text
        if table_text == 'PRODUCTS':
            assert True
            self.logger.info('************* Login procedure is passed *************')
        else:
            self.logger.info('************* Login procedure is failed *************')
            assert False

    def test_logging(self, web_driver):
        self.driver = web_driver

        self.logger.info('************* Test_001_logging *************')
        self.logger.info('************* Verifying loging abilities *************')

        list_of_items_locator = LocatorsFinder().get_locator('list_of_items')
        price_list_locator = LocatorsFinder().get_locator('price_list')

        # получаю список элементов с названиями и ценами товаров
        self.logger.info('************* Getting items name *************')
        item_list = self.driver.find_elements(By.CLASS_NAME, list_of_items_locator)
        price_list = self.driver.find_elements(By.XPATH, price_list_locator)

        # вывожу в логи имя товара
        if item_list and price_list:
            assert True

            for num, element in enumerate(item_list):
                self.logger.info('Printing item name: ')
                self.logger.info(f'{element.text}')

                self.logger.info('Printing item price: ')
                self.logger.info(f'{price_list[num].text}')

            self.logger.info("""************* Oh! I see! Logging is ok, my friend! *************""")

        else:
            self.logger.info("************* Oh, NO!!! Logging is devil's toy! *************")
            self.logger.info("""************* You should never use it again, my friend! *************""")
            assert False
