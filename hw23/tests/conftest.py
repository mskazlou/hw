from selenium import webdriver
import pytest
from selenium.webdriver.chrome.service import Service
from pathlib import Path


s = Service('/home/kms-user/chromedriver/chromedriver')


@pytest.fixture(scope='class')
def web_driver():
    web_driver = webdriver.Chrome(service=s)
    web_driver.maximize_window()
    web_driver.implicitly_wait(3)

    yield web_driver

    web_driver.quit()
    print('\nquit browser!')

