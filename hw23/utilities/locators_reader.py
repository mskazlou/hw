from pathlib import Path


class LocatorsFinder:
    parsed_txt_file_path = Path('..', 'resources', 'SauceDemoLocators.txt')  # указываю путь к текстовому файлу
    data_dict = dict()

    def __init__(self):
        with open(self.parsed_txt_file_path) as file:
            content = list(filter(None, file.read().split('\n')))

        for element in content:
            if element.endswith('_group'):
                key = element
                self.data_dict.update({key: []})
            else:
                self.data_dict[key].append(element)

    def get_login(self):
        for element in self.data_dict['data_group']:
            if element.startswith('login'):
                return element.split('= ')[1]

    def get_pass(self):
        for element in self.data_dict['data_group']:
            if element.startswith('password'):
                return element.split('= ')[1]

    def get_url(self):
        for element in self.data_dict['data_group']:
            if element.startswith('url'):
                return element.split('= ')[1]

    def get_locator(self, locator):
        # Достаю из словаря локатор для искомого элемента
        for key, values in self.data_dict.items():

            for element in values:
                attrib, content = element.split(' = ')
                if attrib == locator:
                    return content


