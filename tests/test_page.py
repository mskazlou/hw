import pytest
from pages.locators import MainPageLocators
from pages.main_page import MainPage
from pages.locators import StoreLocators


class TestMainPage:
    main_page_link = MainPageLocators.URL

    def test_guest_can_go_to_main_page(self, web_driver):
        """checks if quest have access to the correct website."""

        page = MainPage(web_driver, self.main_page_link)

        page.get_page()
        assert page.title() == 'Oscar - Sandbox', "driver didn't get the correct site"

    def test_guest_can_go_to_basket(self, web_driver):
        """checks if quest have access to the basket link."""

        expected_result = 'Your basket is empty. Continue shopping'

        page = MainPage(web_driver, self.main_page_link)

        page.get_the_basket()
        actual_result = page.get_basket_text()

        assert actual_result == expected_result, "Guest's basket should be empty!"


class TestLoginPage:

    main_page_link = MainPageLocators.URL

    def test_guest_can_go_to_login_page(self, web_driver):
        """checks if guest can get the login page. """

        page = MainPage(web_driver, self.main_page_link)
        page.get_page()

        page.go_to_login_page()
        title_name = page.title()
        assert title_name == "Login or register | Oscar - Sandbox", "something goes wrong!"

    def test_should_be_login_url(self, web_driver):
        """checks if the site line is displayed correctly"""

        page = MainPage(web_driver, self.main_page_link)

        assert 'login' in page.current_url()

    @pytest.mark.xfail(reason='just a |is_element_present| flex')
    def test_guest_should_see_login_link(self, web_driver):
        page = MainPage(web_driver, self.main_page_link)

        assert page.should_be_login_link(), "why not?!"


class TestBasketPage:
    store_link = StoreLocators.URL

    def test_guest_have_access_to_the_store(self, web_driver):
        """checks if guest have access to store."""
        page = MainPage(web_driver, self.store_link)
        page.get_page()

        assert page.title() == "All products | Oscar - Sandbox", 'Guest have no access to the store!'

    def test_guest_have_access_to_the_main_page(self, web_driver):
        """checks if we have access to the main page from the store page"""
        page = MainPage(web_driver, self.store_link)
        page.go_to_main_page()

        assert page.title() == "Oscar - Sandbox", "driver didn't get the correct site"

    def test_guest_can_find_goods(self, web_driver):
        """checks if search field works in the correct way"""

        page = MainPage(web_driver, self.store_link)
        page.get_page()

        page.pass_the_keys_to_the_search_field()

        assert page.title() == '"We Are Anonymous" | Oscar - Sandbox', 'something goes wrong'
