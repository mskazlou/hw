# -*- coding: utf-8 -*-
import pytest
# P.S. в файле конфтеста находится весь импорт и фикстуры. в фикстурах возвращаю экземпляры
# тестируемых пользовательских классов (х2).

@pytest.mark.unit
class TestModulsClass:
    """Test Cases for basic moduls"""

    def test_student_name(self, create_student):
        assert create_student.first_name == 'Kozlov', "first name should be Kozlov (p.s. да, не фирст_нэйм, а секонд_нэйм. да-да :) )"

    def test_student_initials(self, create_student):
        assert create_student.initials == 'M.S.'

    def test_student_group_num(self, create_student):

        assert create_student.group_num == 1, 'group num should be equal to 1'

    @pytest.mark.parametrize('student_lesson, exp_result', [
                             ('p.e', 4),
                             ('math', 5),
                             ('literature', 6),
                             ('geography', 7),
                             ('biology', 8)])
    def test_student_marks(self, create_student, student_lesson, exp_result):
        """Тест с mark.parametrize.
        Тест сверяет оценки по ключу ( имени урока)."""

        print('\n')
        print('Nevermind, it"s just a parametrize test data output')
        print(create_student.marks[student_lesson], exp_result)
        assert create_student.marks[student_lesson] == exp_result, 'marks should be equal to 4, 5, 6, 7, 8'

    def test_group_num_count(self, get_school):
        """Тест проверяет количество созданных групп"""
        goups_count = (len(get_school.school_class.keys()))
        assert goups_count == 3, 'groups count should be equal to 3'

    def test_group_names(self, get_school):
        '''Тест проверяет имена созданных групп'''
        expected_result = ['1', '2', '3']
        test_result = []
        for key in get_school.school_class.keys():
            test_result.append(key)
        assert test_result == expected_result, 'group names should be equal to "1", "2", "3" '

    @pytest.mark.xfail
    def test_group_names_fail(self, get_school):

        """Тест для пометки с помощью Xfail"""

        expected_result = ['7', '7', '7']
        test_result = []
        for key in get_school.school_class.keys():
            test_result.append(key)
        assert test_result == expected_result, 'group names should be equal to "1", "2", "3" not to  7 7 7 '


@pytest.mark.integration
class TestSchoolMethods:
    """Test Case for School class methods"""

    def test_average_5_6(self, get_school):
        """тест проверяет имена учеников имеющих средний балл = 5-6"""
        expected_list = ['Kozlov', 'Kurlovich']
        test_list = []
        for value in get_school.school_class.values():
            for student in value:
                average = sum(student.marks.values()) / 5

                if 5 <= average <= 6:
                    test_list.append(student.first_name)

        assert expected_list == test_list, "test_list != 'Kozlov', 'Kurlovich'"

    def test_average_7(self, get_school):
        '''тест проверяет инициалы учеников имеющих средний балл = 7+
        .sorted() использую т.к. словарь - это не упорядоченная коллекция'''
        expected_list = ["M.V.", "A.G.", "O.V.", "O.S."]
        test_list = []

        for value in get_school.school_class.values():
            for student in value:
                average = sum(student.marks.values()) / 5

                if average >= 7:
                    test_list.append(student.initials)

        assert expected_list.sort() == test_list.sort(), '(test_list != "M.V.", "A.G.", "O.V.", "O.S.")'


    def test_get_mates(self, get_school):
        """тест сравнивает учеников одной группы"""
        expected_list = ["Kazakov", "Ignatenko"]
        test_list = []

        group_num = '2'  # Проверяю наличие одноклассников группы №2
        students_group = get_school.school_class[group_num]

        for student in students_group:
            test_list.append(student.first_name)

        assert expected_list.sort() == test_list.sort(), "test_list != 'Ignatenko', 'Kazakov'"


# pytest -s -m integration
# pytest -s -m unit
# pip install -r requirements.txt для установки пакетов. Правда, я сам не понимаю, смысл большей части содержимого в
# requirements.txt
