# -*- coding: utf-8 -*-
# Создайте класс Students, содержащий поля: фамилия и инициалы, номер группы, успеваемость (массив из пяти элементов).
# Создать класс School:
#
# Добавить возможность для добавления студентов в школу
# Добавить возможность вывода фамилий и номеров групп студентов, имеющих оценки, равные только 5 или 6.
# Добавить возможность вывода учеников заданной группы
# Добавить возможность вывода учеников претендующих на автомат(средний балл >= 7)
import random
from string import ascii_lowercase as string    # импортирую eng алфавит в нижнем регистре
lessons = {'p.e': None, 'math': None, 'literature': None, 'geography': None, 'biology': None}


class Student:
    def __init__(self, first_name, initials, group_num, marks):
        self.first_name = first_name
        self.initials = initials
        self.group_num = group_num
        self.marks = marks

    def __repr__(self):
        return f"{self.first_name}, {self.initials}, {self.group_num}, {self.marks}"


class School:
    def __init__(self):
        self.school_class = dict()

    def create_group_num(self, group_name):
        """Метод необходим для создания ключей с номером группы """
        self.school_class.update({str(group_name): set()})

    def add_student(self, student):
        """ Метод добавляет по ключу (номеру группы студента) полные данные студента """
        self.school_class[str(student.group_num)].update({student})

    def average5_6(self):

        for group_name in self.school_class:
            for student in self.school_class[group_name]:
                average = sum(student.marks.values()) / 5

                if 5 <= average <= 6:
                    print(f'Студент {student.first_name} {student.initials} имеет средний бал равный {average}!')

    def get_mates(self, group_num):
        li = list()
        group_num = str(group_num)
        students_group = self.school_class[group_num]
        if students_group:
            print(f'Студент(ы) группы №{group_num}')
            for student in students_group:
                print(student.first_name, student.initials)
        else:
            print(f'Группа №{group_num} пуста!')

    def average_7(self):
        for group_name in self.school_class:
            for student in self.school_class[group_name]:
                average = sum(student.marks.values()) / 5

                if average >= 7:
                    print(f'Студент {student.first_name} {student.initials} имеет средний бал равный {average}!')


SS_5 = School()

for num in range(1, 5):
    SS_5.create_group_num(num)
print(SS_5.school_class)


for _ in range(10):
    SS_5.add_student(
        Student(
            # Случайная фамилия. Первая буква делается заглавной.
            "".join([random.choice(string) for _ in range(7)]).capitalize(),
            # Случайные инициалы.
            "".join([random.choice(string) + chr(46) for _ in range(2)]).upper(),
            # После SS_5.create_group_num в словаре school_class ключи - это номера групп
            # (превращаю в список, ибо на вход надо подать список)
            random.choice(list(SS_5.school_class.keys())),
            # Создаю словарь с рандомными оценками
            {item: random.randint(1, 11) for item in lessons}
        )
    )

print(SS_5.school_class)  # Вывод всех учеников

SS_5.average5_6()   # Вывод учеников, чей средний балл от 5 до 6
print('*' * 40)


group_name = random.randint(1, 4)   # Подаю случайную номер случайной группы в метод
SS_5.get_mates(group_name)      # Вывод учеников одной группы
print('*' * 40)

SS_5.average_7()    # Вывод учеников, чей средний балл выше 7
print('*' * 40)
