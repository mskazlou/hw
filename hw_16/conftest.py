# -*- coding: utf-8 -*-

import pytest
from classes_hw import Student, School

test_list_of_student = {
    Student("Kozlov", "M.S.", 1, {'p.e': 4, 'math': 5, 'literature': 6, 'geography': 7, 'biology': 8}),
    Student("Ignatenko", "M.V.", 1, {'p.e': 7, 'math': 7, 'literature': 7, 'geography': 7, 'biology': 7}),
    Student("Ignatenko", "O.V.", 2, {'p.e': 8, 'math': 8, 'literature': 8, 'geography': 8, 'biology': 8}),
    Student("Kazakov", "A.G.", 2, {'p.e': 9, 'math': 9, 'literature': 9, 'geography': 9, 'biology': 9}),
    Student("Zaiko", "O.S.", 3, {'p.e': 10, 'math': 10, 'literature': 10, 'geography': 10, 'biology': 10}),
    Student("Kurlovich", "A.E.", 3, {'p.e': 6, 'math': 6, 'literature': 6, 'geography': 6, 'biology': 6})}


@pytest.fixture
def create_student():
    '''Фикстура с данными студента'''
    student = Student("Kozlov", "M.S.", 1, {'p.e': 4, 'math': 5, 'literature': 6, 'geography': 7, 'biology': 8})
    return student


@pytest.fixture
def get_school():
    '''Фикстура с готовыми данными для моего тестового класса'''
    school = School()
    for num in range(1, 4):
        school.create_group_num(num)

    for student in test_list_of_student:
        school.add_student(student)
    return school
