import logging
import logging.config
from pathlib import Path

log_file_path = Path('..', 'Logs', 'automation.log')


class LogGen:

    @staticmethod
    def loggen():
        # create logger with 'spam_application'
        logger = logging.getLogger(__name__)
        logger.setLevel(logging.DEBUG)
        # create file handler which logs even debug messages
        fh = logging.FileHandler(log_file_path)
        fh.setLevel(logging.DEBUG)
        # create formatter and add it to the handlers
        formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
        fh.setFormatter(formatter)
        # add the handlers to the logger
        logger.addHandler(fh)

        return logger



