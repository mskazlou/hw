# Разработайте поиск книги в библиотеке
# по ее автору(часть имени)/цене/заголовку/
# описанию.
import xml.etree.ElementTree as ET


def element_finder(root_var, query=None):
    """Функция принимает запрос в строковом формате. Установлен стандартный поиск по писателю.
    Поисковик находит по любомы отрывку содержимого текста. НО ЕСТЬ НЮАНС!
    Поисковик чувствителен к РЕГИСТРУ. Надеюсь, что в смысл данного задания
    не сводится к валидации  :) """
    books_dict = {}
    if query is None:
        query = 'Gambardella'

    for book in root_var.findall('book'):
        for child_element in book:
            if query in child_element.text:
                for key, value in book.attrib.items():
                    key = value
                books_dict.setdefault(key, book[1].text)

    if books_dict:
        print(f'Вашему "{query}" запросу соответствуют: ')
        for key, value in books_dict.items():
            print(f'{key}: {value}')
    else:
        print(f'По Вашему {query} запросу ничего не найдено.')


with open('library.xml') as f:
    root = ET.parse(f).getroot()
    element_finder(root)    # после root можно добавлять свой любой запрос в "str" формате.
    print('*' * 40)
    element_finder(root, 'bad code can function')
    print('*' * 40)
    element_finder(root, 'Computer')    # найдет по 'omputer', но по 'computer' не найдет
    print('*' * 40)
    element_finder(root, '2000')
    print('*' * 40)
    element_finder(root, '-07-07')
    print('*' * 40)
    # element_finder(root, '-07-17')      # Не найдет ни одной книги по данному запросу
    # print('*' * 40)


