import json


# Разработайте поиск учащихся в одном классе, посещающих одну секцию.
# Фильтрацию учащихся по их полу. Поиск ученика по имени(часть имени)


def get_classmates(python_str):
    """Вывод учеников посещающих один класс."""
    classmates_dict = {}

    for student in python_str:
        classmates_dict.setdefault(student['Class'], []).append(student['Name'])

    for key, value in classmates_dict.items():
        print(f"Ученики класса {key}:")
        for studen in value:
            print(studen, end='\t')
        print()


def get_club_output(python_str):
    """Вывод учеников посещающих одну секцию."""
    club_dict = {}
    for student in python_str:
        club_dict.setdefault(student['Club'], []).append(student['Name'])

    for key, value in club_dict.items():
        print(f"Ученики клуба '{key}':")
        for studen in value:
            print(studen, end='\t')
        print()


def values_finder(python_str, query=None):
    """Поиск студентов по значению любого атрибута. Принимает вторым аргументом 'str' формат.
    По стандарту стоит поиск учеников 1b класса.
    """
    list_of_students = []
    if query is None:
        query = '1b'

    for student in python_str:
        for key, value in student.items():
            if query in value:
                list_of_students.append({student["Name"]: student['ID'], key: value})

    if list_of_students:
        print(f'По вашему запросу {query} найден студент: ')
        print(*list_of_students)
    else:
        print(f'По вашему запросу {query} никто не найден!')


def modern_sexism_detector(python_str):
    """Фильтрация учеников по полу."""
    male_list = []
    female_list = []

    for student in python_str:
        if student['Gender'] == 'M':
            male_list.append({student['Name']: student['Gender']})
        else:
            female_list.append({student['Name']: student['Gender']})

    print('Вывожу список студентов мужского рода: ')
    for student in male_list:
        for key, value in student.items():
            print(f'Имя: {key}, пол: {value}')
    print()

    print('Вывожу список студентов женского рода: ')
    for student in female_list:
        for key, value in student.items():
            print(f'Имя: {key}, пол: {value}')


def main():
    with open('students.json') as f:
        python_string = json.load(f)
        values_finder(python_string)
        print('*' * 40)
        values_finder(python_string, 'Haru')    # Поиск по части имени
        print('*' * 40)
        get_classmates(python_string)
        print('*' * 40)
        get_club_output(python_string)
        print('*' * 40)
        modern_sexism_detector(python_string)
        print('*' * 40)


if __name__ == '__main__':
    main()
