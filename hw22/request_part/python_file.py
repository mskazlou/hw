import random
import requests
from requests import api
import json

"""    1) Используя сервис https://fakerestapi.azurewebsites.net и requests библиотеку, написать запросы:
    • GET:
        ◦ Получение списка авторов
        ◦ Получение конкретного автора по его id
    • POST:
        ◦ Добавить новую книгу
        ◦ Добавить нового пользователя
    • PUT:
        ◦ Обновить данные для книги под номером 10
    • DELETE:
        ◦ Удалить пользователя под номером 4
        """


def output_data(response):
    """Не загружаю полученный строковый формат в объект питона,
    так как в этом нет смысла в данном ДЗ.
    Использую всюду метод ради экономии строк кода."""

    if response.ok:
        print(f"Вывожу полученные от сервера данные: ")
        print(response.text)

    else:
        print('Что-то пошло не так!')
        print('Вывожу статус сервера: ')
        print(response.status_code)
    print()


def get_list_of_authors():
    print('Запрашиваю у пользователя данные всех авторов.')
    link = 'https://fakerestapi.azurewebsites.net/api/v1/Authors'
    response = requests.get(link)

    output_data(response)


def get_specific_author():
    random_id = random.randint(1, 322)
    print(f'Запрашиваю у сервера данные автора id{random_id}.')
    link = f'https://fakerestapi.azurewebsites.net/api/v1/Authors/{random_id}'

    response = requests.get(link)
    output_data(response)


def post_new_user():
    print('Создаю нового пользователя id1000.')
    json_user_data = {
        "id": 1000,
        "userName": "string",
        "password": "string"
    }
    link = 'https://fakerestapi.azurewebsites.net/api/v1/Users'
    response = requests.post(link, json=json_user_data)

    output_data(response)


def post_new_book():

    print("Отправляю серверу данные новой книги id1000.")
    json_book_data = {
      "id": 1000,
      "title": "string",
      "description": "string",
      "pageCount": 0,
      "excerpt": "string",
      "publishDate": "2022-01-05T16:38:26.491Z"
    }
    link = 'https://fakerestapi.azurewebsites.net/api/v1/Books'
    response = requests.post(link, json=json_book_data)

    output_data(response)


def refresh_book_data():
    print('Обновляю данные книги id10.')
    json_book_data = {
      "id": 10,
      "title": "string",
      "description": "too much strings to read",
      "pageCount": 322,
      "excerpt": "string",
      "publishDate": "2022-01-05T16:50:23.816Z"
}
    link = "https://fakerestapi.azurewebsites.net/api/v1/Books/10"
    response = requests.put(link, json=json_book_data)
    output_data(response)


def del_user4():

    print('Удаляю юзера id4.')
    link = "https://fakerestapi.azurewebsites.net/api/v1/Users/4"
    response = requests.delete(link)

    if response.text != ' ':
        output_data(response)
        print('Статус код: ')
        print(response.status_code)


def main():

    get_list_of_authors()

    get_specific_author()

    post_new_user()

    post_new_book()

    refresh_book_data()

    del_user4()


if __name__ == '__main__':
    main()
