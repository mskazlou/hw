# * Напечатайте номер заказа
# * Напечатайте адрес отправки
# * Напечатайте описание посылки, ее стоимость и кол-во
# * Сконвертируйте yaml файл в json
# * Создайте свой yaml файл

import json
import yaml
from yaml import FullLoader


def print_bill(yaml_content):
    print('Вывожу номер заказа: ')
    print(yaml_content['invoice'])


def print_address(yaml_content):
    address_lines = yaml_content['bill-to']['address']

    print('Вывожу адресные данные: ')

    for key, values in address_lines.items():
        print(key, values, end='')
    print()


def print_description(yaml_content):
    product_list = yaml_content['product']
    print('Вывожу описание посылки, цену и количество: ')
    for item in product_list:
        print('Description: ', item['description'])
        print('Price: ', item['price'])
        print('Quantity: ', item['quantity'])


with open('order.yaml') as file:
    content = yaml.safe_load(file)
    print_bill(content)
    print('*' * 40)
    print_address(content)
    print('*' * 40)
    print_description(content)
    print('*' * 40)


with open('casual.json', 'w') as json_file:
    """Вывод контента в JSON файл"""
    string_format = yaml.dump(content)

    json.dump(string_format, json_file)

with open('onemore.yaml') as yaml_file:
    """Вывод контента в совершенно новый YAML файл."""
    value = '228322'
    content = yaml.safe_load(yaml_file)
    content['bill-to']['address']['postal'] = value

# создаю YAML файл
content = {
    'a list': [
        1,
        42,
        3.141,
        1337,
        'help',
        u'€'
    ],
    'a string': 'bla',
    'another dict': {
        'foo': 'bar',
        'key': 'value',
        'the answer': 42
    }
}
with open("brand_new_file.yaml", 'w') as new_file:
    yaml.dump(content, new_file)




