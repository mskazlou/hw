from selenium import webdriver
import pytest
from pathlib import Path
from selenium.webdriver.chrome.service import Service


chrome_driver_path = Path('/home/user/Загрузки/chromedriver')
s = Service(chrome_driver_path)


@pytest.fixture(scope="class")
def web_driver():
    web_driver = webdriver.Chrome(service=s)
    web_driver.maximize_window()

    yield web_driver

    web_driver.quit()
