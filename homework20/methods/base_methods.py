import time

from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.remote.webdriver import WebDriver


class PageMethods(object):
    data = ['Ivan', 'Ivanov', '80123456789', 'ivan_iavanov@mail.ru', 'Ivanovskaya St.',
            'Ivanovo', 'I tut Vanya', '123000', 'Ivan', 'qwerty12345', 'qwerty12345']

    def __init__(self, web_driver: WebDriver, url=''):
        self.web_driver = web_driver
        self.get(url)

    def get(self, url):
        self.web_driver.get(url)

    def fields_filler(self):
        input_fields = self.web_driver.find_elements(By.TAG_NAME, 'input')
        input_fields = input_fields[1:12]

        for num, field in enumerate(input_fields):
            field.send_keys(self.data[num])

        submit_btn = self.web_driver.find_element(By.CSS_SELECTOR, 'input[name="submit"]')
        submit_btn.click()

    def user_data_finder(self):
        user_data = []
        user_data_elements = self.web_driver.find_elements(By.CSS_SELECTOR, 'font[size="2"]>b')
        for element in user_data_elements:
            user_data.append(element.text)

        return user_data

    def login_procedure(self):
        current_window = self.web_driver.current_window_handle
        self.web_driver.find_element(By.CSS_SELECTOR, '[rel="nofollow"]').click()
        self.web_driver.switch_to.window(current_window)
        self.web_driver.find_element(By.ID, 'authPhone').send_keys('295620781')
        self.web_driver.find_element(By.ID, 'passwordPhone').send_keys('ytzvbifvyt19')
        self.web_driver.find_element(By.CSS_SELECTOR, 'button[class="button button--action"]').click()
        self.web_driver.find_element(By.CSS_SELECTOR, 'ul.nav__personal li:nth-child(4)>a>span').click()
        self.web_driver.find_element(By.CSS_SELECTOR, 'div li.sidenav__item:nth-child(6)>a').click()
        return self.web_driver.find_element(By.CSS_SELECTOR, 'div.form-static>b').text
