from selenium import webdriver
import pytest
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.by import By


s = Service('/home/kms-user/chromedriver/chromedriver')


@pytest.fixture
def web_driver():
    web_driver = webdriver.Chrome(service=s)
    web_driver.maximize_window()
    web_driver.implicitly_wait(3)

    yield web_driver

    web_driver.quit()
    print('\nquit browser!')

