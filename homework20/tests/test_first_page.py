import time
import pytest
from methods.first_page import FirstPageMethods
from methods.second_page import SecondPageMethods


def test_first_page_user_name(web_driver):
    page = FirstPageMethods(web_driver)
    page.fields_filler()
    user_name = page.user_data_finder()[0]

    assert 'Dear Ivan Ivanov,' == user_name, 'user_name must be equal to Ivan Ivanov'


def test_first_page_user_nickname(web_driver):
    page = FirstPageMethods(web_driver)
    page.fields_filler()
    user_nickname = page.user_data_finder()[1]

    assert 'Note: Your user name is Ivan.' == user_nickname, 'user_nickname must be equal to Ivan'


def test_second_page(web_driver):
    page = SecondPageMethods(web_driver)
    my_email = page.login_procedure()
    assert my_email == 'smittersan@gmail.com', 'my email is smittersan@gmail.com'
